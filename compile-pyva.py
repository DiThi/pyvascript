#!/usr/bin/env python
from pyvascript.grammar import compile
import sys, os

if len(sys.argv) != 2:
    print >>sys.stderr, 'Usage: compile-pyva.py <script>'
    sys.exit(1)

file_name = sys.argv[1].rsplit('.', 1)[0]

fp = open(sys.argv[1], 'r')
source = fp.read()
fp.close()

NANCHECK = 0
CACHE = 1

if CACHE:
    # separate into global snippets to be compiled separately,
    # for caching and smaller compile times
    if not os.path.exists('_cache'):
        os.mkdir('_cache')
    import itertools
    snippets = []
    for p in itertools.groupby(source.split('\n'), lambda x: not x or x[0] in (' ','\t')):
        s = '\n'.join(p[1])+'\n'
        if not p[0] and not s.startswith('except'):
            snippets.append(s)
        elif snippets:
            snippets[-1] += s
    compiled = []
    cont = ""
    for s in reversed(snippets):
        if s[:5] in ('elif ', 'else:'):
            cont = s+cont
            continue
        s = s + cont
        cont = ""
        sh = hex(hash('#nancheck\n'*NANCHECK + s))
        cname = '_cache/c'+sh+'~'
        try:
            out = open(cname).read().decode('utf-8')
        except:
            out = compile(s, NANCHECK)
            open(cname, 'w').write(out.encode('utf-8'))
        compiled.insert(0, out)
    output = '\n'.join(compiled)
else:
    output = compile(source, NANCHECK)
    
new_file = open(file_name + '.js', 'w')
new_file.write(output.encode('utf8'))
new_file.close()
