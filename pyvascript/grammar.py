from pymeta.grammar import OMeta
from pymeta.runtime import ParseError
import json
import os

def compile(source, nancheck=False):
    Translator.nancheck = nancheck
    try:
        return Translator.parse(Grammar.parse(source))
    except ParseError as e:
        if e.message.endswith("expected one of '\\t', or ' '\n"):
            e.message = e.message[:-29]+'inconsistent indentation'
        raise ParseError(e.message)

def p(s):
    print s

try:
    from generated_grammar import PyvaGrammarBase, PyvaTranslatorBase
except:
    grammar_path = os.path.join(os.path.dirname(__file__), 'grammar.ometa')
    translator_path = os.path.join(os.path.dirname(__file__), 'translator.ometa')
    gen_path = os.path.join(os.path.dirname(__file__), 'generated_grammar.py')
    
    pyva_grammar = open(grammar_path, 'r').read()
    pyva_translator = open(translator_path, 'r').read()
    gen = open(gen_path, 'w')
    gen.write('# AUTOMATICALLY GENERATED, remove when modifying .ometa files\n\n')
    gen.write('from pymeta.runtime import OMetaBase as GrammarBase\n\n')
    gen.write('import json\n\n')
    
    from pymeta import builder, grammar, boot
    grammar_base = boot.BootOMetaGrammar(pyva_grammar)
    tree = grammar_base.parseGrammar('PyvaGrammarBase', builder.TreeBuilder)
    gen.write(builder.writePython(tree))
    gen.write('\n')

    translator_base = boot.BootOMetaGrammar(pyva_translator)
    tree = translator_base.parseGrammar('PyvaTranslatorBase', builder.TreeBuilder)
    gen.write(builder.writePython(tree))
    gen.close()
    
    from generated_grammar import PyvaGrammarBase, PyvaTranslatorBase



class Grammar(PyvaGrammarBase):
    keywords = set(('and', 'as', 'break', 'case', 'catch', 'class', 'continue',
        'def', 'default', 'del', 'delete', 'do', 'elif', 'else', 'except',
        'false', 'finally', 'for', 'function', 'if', 'in', 'instanceof',
        '____new', 'not', 'null', 'or', 'pass', 'raise', 'return', '____switch',
        'throw', 'true', 'try', 'typeof', 'var', 'void', 'while', 'with',
        'yield', 'print', 'import', 'from'))
    hex_digits = '0123456789abcdef'

    def __init__(self, *args, **kwargs):
        super(Grammar, self).__init__(*args, **kwargs)
        self.parenthesis = 0
        self.parenthesis_stack = []
        self.indent_stack = [0]

    def enter_paren(self):
        self.parenthesis += 1

    def leave_paren(self):
        self.parenthesis -= 1

    def enter_deflambda(self, indent):
        self.indent_stack.append(indent)
        self.parenthesis_stack.append(self.parenthesis)
        self.parenthesis = 0

    def leave_deflambda(self):
        self.indent_stack.pop()
        self.parenthesis = self.parenthesis_stack.pop()

    def get_indent(self):
        start = self.input.position
        for index in reversed(range(self.input.position)):
            char = self.input.data[index]
            if char == '\n':
                return start - (index + 1)
            elif char != ' ':
                start = index
        return 0

    def dedent(self):
        # A dedent comes after a '\n'. Put it back, so the outer line
        # rule can handle the '\n'
        self.indent_stack.pop()
        input = self.input.prev()
        if input.head()[0] == '\n':
            self.input = input

    def is_keyword(self, keyword):
        return keyword in self.keywords


class Translator(PyvaTranslatorBase):
    op_map = {
        'not': '!',
    }
    binop_map = {
        'or': '||',
        'and': '&&',
        'is': '===',
        'is not': '!==',
    }
    name_map = {
        'None': 'null',
        'True': 'true',
        'False': 'false',
        'self': 'this',
        'int': '_$pyva_int',
        'float': '_$pyva_float',
        'tuple': 'list',
        'unicode': 'str',
    }
    nancheck = False

    def __init__(self, *args, **kwargs):
        super(Translator, self).__init__(*args, **kwargs)
        self.indentation = 0
        self.local_vars = set()
        self.global_vars = set()
        self.var_stack = []
        self.temp_var_id = 0

    def get_name(self, name):
        if name == 'self' and name not in self.global_vars:
            return name
        return self.name_map.get(name, name)

    def make_temp_var(self, name, prefix='$'):
        self.temp_var_id += 1
        return '%s%s%s' % (prefix, self.temp_var_id, name)

    def indent(self):
        self.indentation += 1
        return self.indentation

    def dedent(self):
        self.indentation -= 1

    def is_pure_var_name(self, var):
        return '.' not in var and '[' not in var

    def register_var(self, var):
        if self.is_pure_var_name(var) and var not in self.global_vars:
            self.local_vars.add(var)

    def register_vars(self, vars):
        for var in vars:
            self.register_var(var)

    def register_globals(self, vars):
        self.global_vars.update([var for var in vars if self.is_pure_var_name(var)])
        self.local_vars -= self.global_vars

    def push_vars(self):
        self.var_stack.append((self.local_vars, self.global_vars))
        self.local_vars = set()
        self.global_vars = set()

    def pop_vars(self):
        self.local_vars, self.global_vars = self.var_stack.pop()

    def make_block(self, stmts, indentation):
        indentstr = '  ' * indentation
        sep = '\n%s' % indentstr
        return '{\n%s%s\n%s}' % (indentstr, sep.join(stmts), '  ' * (indentation - 1))

    def make_func_block(self, stmts, indentation):
        indentstr = '  ' * indentation
        sep = '\n%s' % indentstr
        if self.local_vars:
            vars = ', '.join(sorted(self.local_vars))
            var = '%svar %s;\n%s' % (indentstr, vars, indentstr)
        else:
            var = indentstr
        return '{\n%s%s\n%s}' % (var, sep.join(stmts), '  ' * (indentation - 1))

    def make_class_block(self, name, parents, stmts):
        name = name[0][1]
        indentstr = '  ' * self.indentation
        
        parentify = ''
        if parents:
            parentify = indentstr + '%s.prototype = Object.create(%s.prototype);\n' % (name,parents[0])

        cons = """var %s = function(){
            var n = Object.create(%s.prototype);
            n.__init__ && n.__init__.apply(n, arguments);
            return n;};\n""" % (name,name)
        
        for s in list(stmts):
            #if s.startswith('__init__ = '):
                ## s[11:] == u'function(x) {\n  CODE...\n};\n'
                ## TODO: this relies on the syntax of the generated code, it shouldn't
                ## also it should get the code with self instead of this
                #lines = s[11:].replace('this','self').split('\n')
                #lines.insert(1, "var self = Object.create(%s.prototype);" % name)
                #lines.insert(-2, "return self;")
                #cons = 'var %s = %s\n' % (name, '\n'.join(lines))
                ##parentify += '%s.prototype.__init__ = %s\n' % (name, name)
                ##stmts.remove(s)
            if not s:
                stmts.remove(s)
            #if ' = property' in s:
                #stmts.remove(s)
                
        
            
        sep = '\n\n%s' % indentstr
        stpref = sep + name + '.prototype.'
        cons2 = sep + name + '''.__init__ = function(s){
            '''+name+'''.prototype.__init__.apply(s, Array.prototype.slice.call(arguments, 1));
        }'''
            
        #cons += indentstr + '%s.__init__ = %s' % (name, name)
        
        if stmts:
            return sep + cons + parentify + stpref + stpref.join(stmts) \
                   +cons2 #+ sep + '%s.prototype = %s' % (name, name)
        return sep + cons + parentify

    def make_dict(self, items, indentation):
        indentstr = '  ' * indentation
        sep = ',\n%s' % indentstr
        return '{\n%s%s\n%s}' % (indentstr, sep.join(items), '  ' * (indentation - 1))

    def make_if(self, cond, block, elifexprs, elseblock):
        expr = ['if (%s) %s' % (cond, block)]
        expr.extend('else if (%s) %s' % x for x in elifexprs)
        if elseblock:
            expr.append('else %s' % elseblock)
        return ' '.join(expr)

    def make_for(self, var, data, body):
        indentstr = '  ' * self.indentation
        datavar = self.make_temp_var('')
        lenvar = self.make_temp_var('')
        index = self.make_temp_var('')
        init = 'var %s=/*for %s in*/%s, %s=%s.length;' % (
            datavar, var, data, lenvar, datavar)
        body = body.replace('{', '{%s=%s[%s];' % (var, datavar, index), 1)
        return '{init}for(var {index}=0;{index}<{lenvar};{index}++){body}'.format(
            init=init, index=index, lenvar=lenvar, body=body, var=var)

    def make_for_values(self, var, data, body):
        indentstr = '  ' * self.indentation
        datavar = self.make_temp_var('')
        init = 'var %s=/*for %s in*/%s/*.values()*/;' % (datavar, var, data)
        body = body.replace('{', '{%s=%s[%s];' % (var, datavar, var), 1)
        return '{init}for(var {var} in {datavar}){body}'.format(
            init=init, body=body, var=var, datavar=datavar)

    def temp_var_or_literal(self, name, var, init):
        """
        Returns either the literal if it's a literal or a temporary variable
        storing the non-literal in addition to regitering the temporary with
        init.
        """
        if var[0]:
            # Literal
            return var[1]
        temp = self.make_temp_var(name)
        init.append('%s = %s' % (temp, var[1]))
        return temp

    def make_for_range(self, var, for_range, body):
        # for_range is a list of tuples (bool:literal, str:js_code)
        indentstr = '  ' * self.indentation
        stepstr = '%s++' % var
        init = []
        if len(for_range) == 1:
            start = 0
            end = self.temp_var_or_literal('end', for_range[0], init)
        else:
            start = for_range[0][1]
            end = self.temp_var_or_literal('end', for_range[1], init)
            if len(for_range) == 3:
                step = self.temp_var_or_literal('step', for_range[2], init)
                stepstr = '%s += %s' % (var, step)

        initstr = ''
        if init:
            initstr = 'var %s;\n%s' % (', '.join(init), indentstr)

        return '%sfor (%s = %s; %s < %s; %s) %s' % (initstr, var, start, var,
                                                    end, stepstr, body)

    def make_for_reversed_range(self, var, for_range, body):
        indentstr = '  ' * self.indentation
        if len(for_range) == 1:
            return '%s = %s;\n%swhile (%s--) %s' % (var, for_range[0][1], indentstr,
                                                    var, body)

        init = []
        start = for_range[1][1]
        end = self.temp_var_or_literal('end', for_range[0], init)
        if len(for_range) == 3:
            step = self.temp_var_or_literal('step', for_range[2], init)
            stepstr = '%s -= %s' % (var, step)
        else:
            stepstr = '%s--' % var

        initstr = ''
        if init:
            initstr = 'var %s;\n%s' % (', '.join(init), indentstr)

        return '%sfor (%s = (%s) - 1; %s >= %s; %s) %s' % (
            initstr, var, start, var, end, stepstr, body)

    def _prepend_body(self, body, code):
            start = body.index('{') + 1
            code = '\n' + '\n'.join((self.indentation + 1) * '  ' + line
                                    for line in code.split('\n')) + '\n'
            return body[:start] + code + body[start:]

    def make_func(self, name, args, stararg, body):
        func = "function"
        if name:
            name = self.get_name(name[1])
            if name:
                self.register_var(name)
                func = '%s = function' % name
                body += ';'
                
        if args and args[0] == self.get_name('self'):
            args = args[1:]
        clean_args = []
        for arg in reversed(args):
            if isinstance(arg, (tuple, list)):
                arg, default = arg
                body = self._prepend_body(body,
                       'if (typeof %(var)s == "undefined") %(var)s = %(value)s;'
                       % {'var': arg, 'value': default})
            clean_args.insert(0, arg)
        if stararg:
            stararg = self.get_name(stararg)
            self.register_var(stararg)
            body = self._prepend_body(body,
                   'var %s = Array.prototype.slice.call(arguments, %d);'
                   % (stararg, len(args)))
        return '%s(%s) %s' % (func, ', '.join(clean_args), body)
    
    #def set_attr(self, l,r):
        ## property get/setters for IE (work in progress)
        #if l[-1] not in ')]':
            #self.last_obj, self.last_attr = l.rsplit('.',1)
            #if 0:
                #setp = '._set_'.join(l.rsplit('.',1))
                #return "((var _$t={r}),{setp})?({setp}(_$t)):({l} = _$t)".format(**vars())
        #return "%s = %s" % (l, r)

    #def make_property(obj, prop, funcs):
        #print obj, prop, funcs
        


def make_properties(tree):
    t = list(tree)
    try:
        if t[0]=='assign' and t[1][0]=='getattr' and t[2][0]=='call' and t[2][1][1]==u'property':
            tree[0]='call'
            tree[1]=['name','Object.defineProperty']
            t[1][2][0]='string'
            proto = ['getattr',t[1][1],['name','prototype']]
            t[2][2][0][1] = ['getattr',t[2][2][0][1],['name','prototype']]
            t[2][2][1][1] = ['getattr',t[2][2][1][1],['name','prototype']]
            tree[2]=[proto, t[1][2], ['dict', 
                        ['dictkv', ['string', u'get'], t[2][2][0] ],
                        ['dictkv', ['string', u'set'], t[2][2][1] ]]]
            return tree
    except:
        pass
    for e in tree:
        if isinstance(e,list):
            make_properties(e)
    return tree

